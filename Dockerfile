FROM debian:bullseye


# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
 php-cli                    \
 php-common                 \
 php-composer-ca-bundle     \
 php-composer-semver        \
 php-composer-spdx-licenses \
 php-composer-xdebug-handler\
 php-curl                   \
 php-fpm                    \
 php-geoip                  \
 php-igbinary               \
 php-imagick                \
 php-json-schema            \
 php-mbstring               \
 php-mysql                  \
 php-pgsql                  \
 php-psr-log                \
 php-redis                  \
 php-symfony-console        \
 php-symfony-debug          \
 php-symfony-filesystem     \
 php-symfony-finder         \
 php-symfony-process        \
 php-xml                    \
 php-zip                    \
 php7.4-cli                 \
 php7.4-common              \
 php7.4-curl                \
 php7.4-fpm                 \
 php7.4-gmp                 \
 php7.4-json                \
 php7.4-mbstring            \
 php7.4-mysql               \
 php7.4-opcache             \
 php7.4-pgsql               \
 php7.4-readline            \
 php7.4-sqlite3             \
 php7.4-xml                 \
 php7.4-zip                 \
 nodejs npm nginx && \
 apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp

ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=048b95b48b708983effb2e5c935a1ef8483d9e3e

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

RUN sed -i "s/^listen = .*/listen = 9000/"  /etc/php/7.4/fpm/pool.d/www.conf  && mkdir -p /run/php/

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php --install-dir=/usr/local/bin --filename=composer && chmod +x /usr/local/bin/composer

# Set working directory
WORKDIR /var/www

